import React from 'react'

export const Button = ({title, onClick = null, className}) => {
    return (
        <div className={className} onClick={onClick}>
            {title}
        </div>
    )
}
