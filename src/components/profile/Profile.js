import React from 'react'

const Profile = ({url, name, role, badges}) => {
    return (
        <div className='single-guest'>
            <img src={url} className="profile-pic" alt="" />
            <p>{name}</p>
            {role && <p>{role || 'Not Availabel'}</p>}
            {badges && <p>{badges[0]}</p>}
        </div>
    )
}

export default Profile;