import React,{ useState } from 'react'
import Icon from '../icon/Icon'

export const Search = ({icon_type, onClick=null}) => {
    // search value state
    let [ searchValue, setSearchValue ] = useState("")

    const onChange = (e) => {
        setSearchValue(e.target.value)
    }

    return (
        <div className='filter-search'>
            <input type='search' placeholder='Search by name' value={searchValue} onChange={onChange}/>
            <div className="search_button" onClick={()=>onClick(searchValue)}>
                <Icon title={icon_type} icon_class="search_icon"/>
            </div>
        </div >
    )
}
