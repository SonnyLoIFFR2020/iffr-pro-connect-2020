import React from 'react'

import Icon from '../icon/Icon'
import { Link } from 'react-router-dom'

const Header = ({title}) => {
    return (
        <div className="header">
            <Link to='/' className='header-link'>
                <Icon title="logo" icon_class="header_logo" />
                <h2>{title}</h2>
            </Link>
        </div>
    )
}

export default Header