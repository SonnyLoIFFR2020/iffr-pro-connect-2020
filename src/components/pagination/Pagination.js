import React,{useState, useEffect, useContext} from 'react'
import Data from '../../data/data.json'

import {CompanyContext} from '../../context/CompanyProvider'

export const Pagination = ({postsPerPage, totalPosts, paginate}) => {

   const pageNumbers =[]

   let list = useContext(CompanyContext)

   for(let i=0; i <= Math.ceil(totalPosts/postsPerPage); i++){
       pageNumbers.push(i)
   }

    return (
        <div className='page_numbers'>
            <ul className="pagination">
                {pageNumbers.map(number =>(
                    <li key={number} className="pagination_number">
                        <a onClick={()=>paginate(number)} href='!#' className="pagination_link">
                            {number}
                        </a>
                    </li>
                ))}
            </ul>
        </div>
    )
}
