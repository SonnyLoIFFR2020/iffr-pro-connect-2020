import React, { useState} from 'react'

import Icon from '../icon/Icon'


export default function Dropdown(props) {

    const { classTitle, title, options, filter } = props

    let [ open, setOpen ] = useState(false)

    return (
        <div className={`dd ${classTitle} ${open ? "dd-open" : ""}`}  onClick={() => setOpen(open = !open)}>
            <div className='dd-header'>
                <h3>{title}</h3>
                <Icon title="down_arrow" icon_class="down_arrow" />
            </div>
            <ul className={`dd-list`}>
                {options.map(
                    (data, index) => 
                        {   
                            if(data.value !== ""){
                                return(
                                    <li key={index} className='dd-list-item' data-value={data.value} onClick={filter}>{data.label}</li>
                                )
                            }
                            return null;
                        }
                    )
                }
            </ul>
        </div>
    )
}