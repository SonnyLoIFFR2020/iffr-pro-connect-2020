import React, {useState} from 'react'

import DropDown from './Dropdown'
import {Button} from '../button/Button'
import {Search} from '../search/Search'
import Icon from '../icon/Icon'

import { CompanyConsumer } from '../../context/CompanyProvider' 

const Filter = () => {


    let [ open, setOpen ] = useState(false);

    return (
        <CompanyConsumer>
            { value => {
                    const {countryFilter, badgeFilter, sectorFilter, countryList, badgeList, sectorList, list_sort, findGuest } = value

                    return(
                        <>
                        <nav className="filter">
                            <form className='filter-input'>
                                <Button title="A-Z" className="sort_button" onClick={list_sort} />
                                <Search icon_type='search' onClick={findGuest} />
                            </form>
            
                            <form className="filter-dd">
                                <DropDown classTitle="dd-country" title="Country" options={countryList} filter={countryFilter} />
                                <DropDown classTitle="dd-badges" title="Badges" options={badgeList} filter={badgeFilter} />
                                <DropDown classTitle="dd-sector" title="Sector" options={sectorList} filter={sectorFilter} />
                            </form>
                        </nav>
            
                        <nav className={`filter-mobile ${open ? "filter-mobile-open" : "" }`}>
                            <div className="filter-option" onClick={() => setOpen(open = !open)}>
                                <h3>Filter</h3>
                            </div>
                            <form>
                                <input type='search' className="dd-search"/>
                                <DropDown classTitle="dd-country" title="Country" options={countryList} filter={countryFilter} />
                                <DropDown classTitle="dd-badges" title="Badges" options={badgeList} filter={badgeFilter} />
                                <DropDown classTitle="dd-sector" title="Sector" options={sectorList} filter={sectorFilter} />
                            </form>
                        </nav>
                    </>
                    )
                } 
            }
        </CompanyConsumer>

    )
}

export default Filter