import React from 'react'

import Icon from '../icon/Icon';
import Profile from '../profile/Profile'

import logo from '../../assets/image/logo.svg';

import { Link } from 'react-router-dom';

// import { CompanyConsumer } from '../../context/CompanyProvider'

const Card = (props) => {

    const {
        from,
        id,
        company,
        mainWorkingSector,
        otherWorkingSector,
        territories,
        country,
        email,
        website,
        companyProfile,
        attendingGuests,
    } = props


    if(from === 'main'){
        return(
            <>
                <div className="company-main-upper">
                    <Icon title='star' icon_class='star'/>
                    <h3>{company}</h3>
                </div>
                <div className="company-main-lower">
                    <Link to={`/${id}`}>
                            <div className='card-info'>
                                <p><strong>Country:</strong> {country || 'Not Available'}</p>
                                <p><strong>Website:</strong> {website || 'Not Available'}</p>
                                <p><strong>Email:</strong> {email || 'Not Available'}</p>
                                <p><strong>Main Sector:</strong> {mainWorkingSector || 'Not Available'}</p>
                            </div>
                            <div className="card-guests">
                                {
                                    attendingGuests.slice(0, 2).map( (guest, index) => {
                                        return(
                                            <Profile key={index}
                                            url={logo}
                                            name={guest.name}
                                        />
                                        )
                                    })
                                }
                            </div>
                    </Link>
                </div>
            </>
        )
    }else if(from === "company"){
        return(
            <>
                <div className="company-profile-upper">
                    <Icon title="star" icon_class="star"/>
                    <h1>{company}</h1>
                </div>
                <div className="company-profile-mid">
                    <div className="company-profile-mid-left">
                        <h2>Company Profile</h2>
                        <p>{companyProfile}</p>
                    </div>
                    <div className="company-profile-mid-right">
                        <div className="company-profile-mid-right-details">
                            <p><strong>Country:</strong> {country || 'Not Available'}</p>
                            <p><strong>Website:</strong> {website || 'Not Available'}</p>
                            <p><strong>Email:</strong> {email || 'Not Available'}</p>
                            <p><strong>Main Sector:</strong> {mainWorkingSector || 'Not Available'}</p>
                            <p><strong>Other working Sector:</strong> {otherWorkingSector || 'Not Available'}</p>
                            <p><strong>Territories:</strong> {territories || 'Not Available'}</p>
                        </div>
                        <div className="company-profile-mid-right-icons">
                            <Icon title="export" icon_class="redirect"/>
                        </div>
                    </div>
                </div>
                <div className="company-profile-guests">
                    <h3>Attending Guests</h3>
                    <div className="company-profile-guests-profile">
                        {
                            attendingGuests.map((guest, index) => (
                                <Profile key={index}
                                    url={logo}
                                    name={guest.name}
                                    role={guest.role}
                                    badges={guest.badges}
                                />
        
                            ))
                        }
                    </div>
                </div>
            </>
        )
    }


    // if(from === 'main'){
    //     return(

    //         <CompanyConsumer>
    //             {
    //                 value => {
    //                     const { filterList } = value
    //                     if(filterList === undefined){
    //                         return(
    //                             <h1>Nothing to show</h1>
    //                         )
    //                     }
    //                     return(            
    //                         filterList.map((card, index) => {
    //                             return(
    //                                 <div key={index} id={card.id} className='company-main'>
    //                                         <div className="company-main-upper">
    //                                             <Icon title='star' icon_class='favourite'/>
    //                                             <h3>{card.company}</h3>
    //                                         </div>
    //                                         <div className="company-main-lower">
    //                                             <Link to={`/${card.id}`}>
    //                                                     <div className='card-info'>
    //                                                         <p><strong>Country:</strong> {card.country || 'Not Available'}</p>
    //                                                         <p><strong>Website:</strong> {card.website || 'Not Available'}</p>
    //                                                         <p><strong>Email:</strong> {card.email || 'Not Available'}</p>
    //                                                         <p><strong>Main Sector:</strong> {card.mainWorkingSector || 'Not Available'}</p>
    //                                                     </div>
    //                                                     <div className="card-guests">
    //                                                         {
    //                                                             card.attendingGuests.slice(0, 2).map( (guest, index) => {
    //                                                                 return(
    //                                                                     <Profile key={index}>
    //                                                                         <div className="single-guest">
    //                                                                             <img src={logo} className="profile-pic" alt=""/>
    //                                                                             <p>{guest.name}</p>
    //                                                                         </div>
    //                                                                     </Profile>
    //                                                                 )
    //                                                             })
    //                                                         }
    //                                                     </div>
    //                                             </Link>
    //                                         </div>
    //                                 </div>
    //                             )
    //                         })
    //                     )
    //                 }
    //             }
    //         </CompanyConsumer>
    //     )
    // }

    // const main_card = data.map((card, index) => {
    //     return(
    //             <div key={index} id={card.id} className='company'>
    //                 <div className="company-upper">
    //                     <Icon title='star' icon_class='favourite'/>
    //                     <h3>{card.company}</h3>
    //                 </div>
    //                 <div className="company-lower">
    //                     <div className='card-info'>
    //                         <p><strong>Country:</strong> {card.country || 'Not Available'}</p>
    //                         <p><strong>Website:</strong> {card.website || 'Not Available'}</p>
    //                         <p><strong>Email:</strong> {card.email || 'Not Available'}</p>
    //                         <p><strong>Main Sector:</strong> {card.mainWorkingSector || 'Not Available'}</p>
    //                     </div>
    //                     <div className="card-guests">
    //                         {
    //                             card.attendingGuests.map( (guest, index) => {
    //                                 return(
    //                                     <Profile key={index}>
    //                                         <div className="single-guest">
    //                                             <img src={logo} className="profile-pic" alt=""/>
    //                                             <p>{guest.name}</p>
    //                                         </div>
    //                                     </Profile>
    //                                 )
    //                             })
    //                         }
    //                     </div>
    //                 </div>
    //             </div>
    //     )
    // })

    
    // if(type==="main"){
    //     return(
    //         <>
    //             {main_card}
    //         </>
    //     )
    // }else if(type==="company-view"){

    // }
}

export default Card