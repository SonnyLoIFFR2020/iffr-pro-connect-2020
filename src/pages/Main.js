import React from 'react'

import Filter from '../components/dropdown/Filter'
import Card from '../components/card/Card';

// import { Pagination } from '../components/pagination/Pagination'
import { CompanyConsumer } from '../context/CompanyProvider';

const Main = () => {


    // const [ currentPage, setCurrentPage ] = useState(1)
    // const [ postPerPage ] = useState(50)


    // const indexOfLastPost = currentPage * postPerPage
    // const indexOfFirstPost = indexOfLastPost - postPerPage
    // const currentPosts = newList.slice(indexOfFirstPost, indexOfLastPost)

    // const paginate = (pageNumber) => setCurrentPage(pageNumber)

    // return (
    //     <>
    //         <Filter/>
    //         <div className="companies">
    //             <Card type='main' />               
    //             {/* <Card type='main' data={currentPosts}/>                */}
    //         </div>
    //         {/* <Pagination postsPerPage={postPerPage} totalPosts={newList.length} paginate={paginate}/> */}
    //     </>
    // )

    return(
        <>
            <Filter />
            <div className="companies">
                <CompanyConsumer>
                    {
                        value => {
                            const {filterList} = value

                            if(filterList === undefined) {
                                return(
                                    <h1>Nothing to show here</h1>
                                )
                            }

                            return(
                                filterList.map((card, index) => {
                                    return(
                                        <div key={index} className='company-main'>
                                            <Card from="main"
                                                id={card.id}
                                                company={card.company}
                                                country={card.country}
                                                website={card.website}
                                                email={card.email}
                                                mainWorkingSector={card.mainWorkingSector}
                                                attendingGuests={card.attendingGuests}
                                            />
                                        </div>
                                    )
                                })
                            )

                        }
                    }
                </CompanyConsumer>
            </div>
            
        </>
    )
}

export default Main;