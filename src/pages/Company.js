import React from 'react'
import { useParams } from 'react-router-dom'

import {CompanyConsumer} from '../context/CompanyProvider'

import Card from '../components/card/Card'

export const Company = () => {

    let { id } = useParams();

    return (
        <div className="company-profile">
            <CompanyConsumer>
                {
                    value => {
                        const {filterList} = value
                        const company = filterList.filter(company => company.id === id)

                        return(
                            <Card
                                from="company"
                                id={id}
                                company={company[0].company}
                                mainWorkingSector={company[0].mainWorkingSector}
                                otherWorkingSector={company[0].otherWorkingSector}
                                territories={company[0].territories}
                                country={company[0].country}
                                email={company[0].email}
                                website={company[0].website}
                                companyProfile={company[0].companyProfile}
                                attendingGuests={company[0].attendingGuests}
                            />
                        )
                    }
                }

            </CompanyConsumer>
        </div>
    )
}
