import React from 'react';
import ReactDOM from 'react-dom';
import './App.scss';
import App from './App';

import {BrowserRouter as Router} from 'react-router-dom'

import { CompanyProvider } from './context/CompanyProvider'

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <CompanyProvider>
        <App />
      </CompanyProvider>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);
