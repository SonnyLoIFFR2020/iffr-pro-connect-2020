import React, {useState} from 'react'
import Data from '../data/data.json'

// Create context
export const CompanyContext = React.createContext()

const CompanyProvider = (props) => {

    // initialize the starting list state with all the data from the json. 
    const list = Data.data.map(d=>d);

    // Set filterable state
    let [filterList, setFilterList] = useState(list)

    // Retrieve all the country data, put them in a label and value, retrieve only unique set and then sort
    const countries = [...new Set(list.map(data =>data.country).sort())].map(data=>({label: data, value: data}))
    const [countryList] = useState([{label: "All", value: "All"},...countries])

    // Retrieve all the sector data, put them in a label and value, retrieve only unique set and then sort
    const sector = [...new Set(list.map(data=>data.mainWorkingSector).sort())].map(data => ({label: data, value: data}))
    const [sectorList] = useState([{label: "All", value: "All"},...sector]) 

    // Retrieve all the filterbadges from the list and put only unique values using Set, sort them, and map them to label and value.
    const filterbadges = [...new Set(list.map(badge => badge.filterBadges[0]).sort())].map(data=>({label: data, value: data}))
    // Set the filterbadges as a state
    const [badgeList] = useState([{label: 'All', value: 'All'}, ...filterbadges])

    // Check wether the list is sorted alphabetically
    let [ sorted, setSorted ] = useState(true)

    // search value state
    let [ searchValue, setSearchValue ] = useState("")


    /*
    * Methods of the context provider
    */

   // Filter function for country
    const countryFilter = (e) => {
      if(e.target.dataset.value === 'All'){
        setFilterList(list)
      }else{
        setFilterList(list.filter(data => data.country === e.target.dataset.value))
      }
    }

    // Filter function for badge
    const badgeFilter = (e) => {
      if(e.target.dataset.value === 'All'){
        setFilterList(list)
      }else{
        setFilterList(list.filter(data => data.filterBadges[0] === e.target.dataset.value))
      }
    }

    // Filter function for sector
    const sectorFilter = (e) => {
      if(e.target.dataset.value === 'All'){
        setFilterList(list)
      }else{
        setFilterList(list.filter(data => data.mainWorkingSector === e.target.dataset.value))
      }
    }

    // Sort the list alphabetically A-Z or Z-A on company name based on the boolean
    const list_sort = (e) => {
      if(sorted){
        setFilterList([...filterList].sort((a,b)=>{
          if(a.company.toLowerCase() > b.company.toLowerCase()) return -1;
          if(a.company.toLowerCase() < b.company.toLowerCase()) return 1;
          return 0;
        }))
        setSorted(false)
      }else{
        setFilterList([...filterList].sort((a,b)=>{
          if(a.company.toLowerCase() < b.company.toLowerCase()) return -1;
          if(a.company.toLowerCase() > b.company.toLowerCase()) return 1;
          return 0;
        }))
        setSorted(true)
      }
    }

    const findGuest = (name) => {
      if(name.trim().length > 0){
        setFilterList(filterList.filter(data => data.country === name))
      }else{
        setFilterList(list)
      }
    }

    return(
        <CompanyContext.Provider value={{
            filterList,
            setFilterList,
            countryList,
            sectorList,
            badgeList,
            searchValue,
            countryFilter,
            badgeFilter,
            sectorFilter,
            list_sort,
            findGuest
        }}>
            {props.children}
        </CompanyContext.Provider>
    )
}

// Create consumer
const CompanyConsumer = CompanyContext.Consumer

export {CompanyProvider, CompanyConsumer}