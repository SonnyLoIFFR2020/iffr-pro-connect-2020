import React from 'react';

import Header from './components/header/Header';
import Main from './pages/Main';
import {Company} from './pages/Company';
import {Default} from './pages/Default';

import { Switch, Route } from 'react-router-dom'

function App() {

  return( 
    <>
      <Header title='Pro Connect' />
      <div className="App">
        <Switch>
          <Route exact path='/' component={Main}/>
          <Route exact path='/:id' component={Company} />
          <Route component={Default}/>
        </Switch>
      </div>
    </>

  )

}

export default App;